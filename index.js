const LANGUAGE = {
    HINDI: {
        code: 'hi',
        locale: 'hi_IN',
        __name__: 'HINDI'
    },
    TAMIL: {
        code: 'ta',
        locale: 'ta_IN',
        __name__: 'TAMIL'
    },
    MALAYALAM: {
        code: 'ml',
        locale: 'ml_IN',
        __name__: 'MALAYALAM'
    }
}

const express = require('express')
const rp = require('request-promise')

const app = express()

app.set('view engine', 'ejs');

app.get('/', (req, res, next) => res.json({hello: "world"}))

app.get('/public', (req, res) => {
    return res.sendFile(__dirname + '/public/working.html')
})

app.get('/pratilipi', async (req, res, next) => {
    if (!(req.query.language && req.query.slug)) {
        return res.status(400).json({message: 'language and slug to be passed in slug'})
    }
    if (!(LANGUAGE[req.query.language])) {
        return res.status(400).json({message: 'language not supported or incorrect'})
    }

    const pratilipi = await rp(`https://${req.query.language.toLowerCase()}.pratilipi.com/api/pratilipis?slug=${req.query.slug}&accessToken=dc24ea5b-6e79-4a9d-ab85-56a342a7aab2`).then(JSON.parse).catch(() => null)

    if (!pratilipi) {
        return res.status(400).json({message: 'invalid slug'})
    }

    return res.render('pratilipi', {
        lang_code: LANGUAGE[req.query.language]['code'],
        locale: LANGUAGE[req.query.language]['locale'],
        language: LANGUAGE[req.query.language]['__name__'],
        page_title: pratilipi['displayTitle'] + ' -- ' + pratilipi['author']['displayName'],
        og_title: pratilipi['displayTitle'],
        image_url: pratilipi['coverImageUrl'].replace('&', '&amp;') + '&amp;width=620&amp;enforceAspectRatio=false',
        description: pratilipi['summary'],
        content_link: `https://${req.query.language.toLowerCase()}.pratilipi.com${pratilipi['pageUrl']}`
    })
})

app.get('/series', async (req, res, next) => {
    if (!(req.query.language && req.query.slug)) {
        return res.status(400).json({message: 'language and slug to be passed in slug'})
    }
    if (!(LANGUAGE[req.query.language])) {
        return res.status(400).json({message: 'language not supported or incorrect'})
    }

    const asd = `https://${req.query.language.toLowerCase()}.pratilipi.com/api/series/v1.0?slug=${req.query.slug}&accessToken=dc24ea5b-6e79-4a9d-ab85-56a342a7aab2`
    console.log(asd)

    const series = await rp(asd).then(JSON.parse).catch(() => null)

    if (!series) {
        return res.status(400).json({message: 'invalid slug'})
    }

    return res.render('series', {
        lang_code: LANGUAGE[req.query.language]['code'],
        locale: LANGUAGE[req.query.language]['locale'],
        language: LANGUAGE[req.query.language]['__name__'],
        page_title: series['displayTitle'] + ' -- ' + series['author']['displayName'],
        og_title: series['displayTitle'],
        image_url: series['coverImageUrl'].replace('&', '&amp;') + '&amp;width=620&amp;enforceAspectRatio=false',
        description: series['author']['displayName'], 
        summary: series['summary'],
        content_link: `https://${req.query.language.toLowerCase()}.pratilipi.com/series/${series['pageUrl']}`
    })
})

app.listen(8080)
